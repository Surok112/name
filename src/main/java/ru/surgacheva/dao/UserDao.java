package ru.surgacheva.dao;

import javax.ejb.Stateless;
import ru.surgacheva.model.User;

@Stateless
public class UserDao extends GenericDaoJpa<User, String>{

    public UserDao() {
        super(User.class);
    }
}