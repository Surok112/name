package ru.surgacheva.model;

public enum UserRole {
    ADMIN,
    USER
}